package com.penngo.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;


/**
 * 
CREATE TABLE [useCase] (
[id] INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL,
[pid] INTEGER  NOT NULL, 
[name] VARCHAR(50)  NULL,
[url] VARCHAR(200)  NULL,
[method] VARCHAR(10)  NULL,
[type] VARCHAR(50)  NULL,
[code] vaRCHAR(20)  NULL,
[request] TEXT  NULL,
[response] TEXT  NULL,
[createTime] VARCHAR(20)  NULL,
[dataSort] INTEGER DEFAULT '0'' NULL,
[caseTime] INTEGER DEFAULT '''0''' NULL,
[assertValue] TEXT  NULL
)
 *
 */
public class UseCase  extends Model<UseCase> {
	public final static String TYPE = "useCase";
	public final static UseCase model = new UseCase();

	public List<UseCase> findAllByPid(String pid){
		List<UseCase> list = UseCase.model.find(
				"select * from useCase where pid = ? ", pid);
		return list;
	}
	public static void main(String[] args){
		String table = "sss_aa";
		System.out.println(table.substring(0, table.indexOf("_")));
	}
}
